package mypackage;

import java.util.Arrays;

public class Deal {
    private Hand player1;
    private Hand player2;

    public Deal(String pack){
        String[] cardsOnDeck = pack.split(" ");
        this.player1 =  new Hand(Arrays.asList(Arrays.copyOfRange(cardsOnDeck, 0,5)));
        this.player2 =  new Hand(Arrays.asList(Arrays.copyOfRange(cardsOnDeck, 5,10)));
    }

    public int evaluate(){
        player1.sortHand();
        player2.sortHand();


        if (!(player1.equals(player2))){
            return player1.compareTo(player2);
        }

        return player1.tieBreaker(player2, player1.getMaxValue());
    }

}
