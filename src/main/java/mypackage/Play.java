package mypackage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Play {


    public static void main(String[] args) {

        int numOfWinsByPlayer1=0;
        int numOfWinsByPlayer2=0;

        BufferedReader bufferedReader;

        String line;

        Deal deal;

        int dealResult;

        try {
            bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            while ((line = bufferedReader.readLine()) != null) {

                deal = new Deal(line);
                dealResult=deal.evaluate();

                if (dealResult == 1) {
                    numOfWinsByPlayer1++;
                } else if (dealResult == -1) {
                    numOfWinsByPlayer2++;
                } else {
                    System.out
                            .println("This deal was a tie" + line);
                }
            }

            System.out.println("Player 1: " + numOfWinsByPlayer1 + " hands");
            System.out.println("Player 2: " + numOfWinsByPlayer2 + " hands");

            System.exit(0);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
        }


