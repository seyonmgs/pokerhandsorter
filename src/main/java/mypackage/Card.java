package mypackage;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Card {
    private int value;
    private String suit;

    public Card(String s){

        switch (s.substring(0,1)) {
            case "T":
                this.value = 10;
                break;
            case "J":
                this.value = 11;
                break;
            case "Q":
                this.value = 12;
                break;
            case "K":
                this.value = 13;
                break;
            case "A":
                this.value = 14;
                break;
            default:
                this.value = Integer.parseInt(s.substring(0,1));
                break;
        }

        this.suit = s.substring(1,2);
    }

}
