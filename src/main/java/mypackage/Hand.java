package mypackage;

import lombok.Getter;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;

public class Hand implements Comparable<Hand> {

    private @Getter long rank;
    private @Getter long subRank;
    private @Getter long tieBreaker;

    private Map<Integer,List<Card>> valueMap;
    private Map<String,List<Card>>  suitMap;
    private Set<Integer> valueSet;
    private Set<String> suitSet;
    private List<Card> listOfCards;
    private @Getter long maxValue;
    private long minValue;

    public Hand(List<String> cards){
        this.listOfCards = cards.stream()
                .map(string -> new Card(string))
                .collect(toList());

        this.valueMap = listOfCards.stream()
                .collect(Collectors.groupingBy(card -> card.getValue(), mapping(card -> card, toList())));

        this.suitMap= listOfCards.stream()
                .collect(Collectors.groupingBy(card -> card.getSuit(), mapping(card -> card, toList())));

        this.valueSet=valueMap.keySet();
        this.suitSet=suitMap.keySet();

        this.rank=0;
        this.subRank=0;
        this.tieBreaker=0;

         if (valueSet.size()>0){
             this.maxValue=valueSet.stream().mapToInt(m->m).max().getAsInt();
         }else{
            this.maxValue=0;
        }


        if (valueSet.size()>0){
            this.minValue=valueSet.stream().mapToInt(m->m).min().getAsInt();
        }else{
            this.minValue=0;
        }

    }

    public void sortHand(){
            /*
            check for straight flush
             */
            if ((this.valueSet.size() == 5) && (this.maxValue-this.minValue == 4) && (this.suitSet.size() ==1)) {
                    this.rank=9;
                    this.subRank=this.maxValue;
                    /*
                    check for Royal Flush
                     */
                    if (this.maxValue==13){
                        this.rank=10;
                        this.subRank=100;
                    }


                    return;
            }


            /*
            check for four of a kind
             */

            Optional<Card> fourOfAKindCard = this.valueMap.entrySet().stream().filter(entry -> entry.getValue().size() == 4)
                    .map(entry -> entry.getValue().get(0))
                    .findAny();

            if ((this.valueSet.size() == 2) && fourOfAKindCard.isPresent()){

                        this.rank=8;
                        this.subRank=fourOfAKindCard.get().getValue();

                        /*
                        There are only two possible values
                         */
                        this.setTieBreakerSimple();
                        return;
            }

             /*
            check for full house
             */

            Optional<Card> threeOfAKindCard = this.valueMap.entrySet().stream().filter(entry -> entry.getValue().size() == 3)
                    .map(entry -> entry.getValue().get(0))
                    .findAny();

            if ((this.valueSet.size() == 2) && threeOfAKindCard.isPresent()){

                this.rank=7;
                this.subRank=threeOfAKindCard.get().getValue();

                /*
                 There are only two possible values
                 */
                this.setTieBreakerSimple();

                return;

            }

             /*
            check for flush
             */

            if ((this.suitSet.size() == 1)){

                    this.rank=6;
                    this.subRank=this.maxValue;
                    return;

             }


             /*
            check for straight
             */

            if ((this.suitSet.size() == 1) && (this.maxValue - this.minValue == 4)){

                this.rank=5;
                this.subRank=this.maxValue;
                return;

            }


             /*
            check for three of a kind
             */

             if (threeOfAKindCard.isPresent()){
                 this.rank=4;
                 this.subRank=threeOfAKindCard.get().getValue();
                 return;
             }


             List<Card> listOfPairs = this.valueMap.entrySet().stream().filter(entry -> entry.getValue().size() == 2)
                     .map(entry -> entry.getValue().get(0))
                     .collect(toList());

             /*
            check for two different pairs
             */

             if(listOfPairs.size() == 2){
                 this.rank=3;
                 this.subRank=listOfPairs.stream().map(Card::getValue).mapToLong(m->m).count();
                 return;
             }

             /*
            two cards of same value
             */

            if(listOfPairs.size() == 1){
                this.rank=2;
                this.subRank=listOfPairs.get(0).getValue();
                return;
            }

            /*
            highest value card
             */

            this.rank=1;
            this.subRank=this.maxValue;
            return;


    }

    public void setTieBreakerSimple(){
        /*
         There are only two possible values
        */
        if (this.maxValue == this.subRank){
            this.tieBreaker=this.minValue;
        }else {
            this.tieBreaker=this.maxValue;
        }

        return;
    }

    public Long getNextHighestValue(Long currentMaxValue){
        OptionalLong nexvalue =valueSet.stream().filter(value -> value < currentMaxValue)
                .mapToLong(m->m).max();
        if(nexvalue.isPresent()){
            return nexvalue.getAsLong();
        }
        return 0L;
    }

    @Override
    public boolean equals (Object o ){

        if (o == null) {
            return false;
        }

        if (o == this) {
            return true;
        }

        if (!(o instanceof Hand)) {
            return false;
        }

        Hand other= (Hand) o;

        if (this.rank !=  (other.getRank())){
            return new Long(this.rank).equals(new Long(other.getRank()));
        }

        return new Long(this.subRank).equals(new Long(other.getSubRank()));

    }

    @Override
    public int compareTo(Hand o) {
        if (this.rank != o.getRank()){
            return new Long(this.rank).compareTo(new Long(o.getRank()));
        }

        if (this.subRank != o.getSubRank()){
            return new Long(this.subRank).compareTo(new Long(o.getSubRank()));
        }

        return  new Long(this.tieBreaker).compareTo(new Long(o.getTieBreaker()));
    }


    public int tieBreaker(Hand player2, long max){

        long player1NextHighestValue = this.getNextHighestValue(max);
        long player2NextHighestValue = player2.getNextHighestValue(max);

        if ( player1NextHighestValue != player2NextHighestValue){
            return  new Long(player1NextHighestValue).compareTo(new Long(player2NextHighestValue));
        }

        if ( player1NextHighestValue !=0){
            return this.tieBreaker(player2, player1NextHighestValue);
        }

        return 0;

    }
}
